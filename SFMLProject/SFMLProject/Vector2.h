#pragma once
/////////////////////////////////////////////////////////////////////////////
//
// class Vector2 � a simple 2D vector class
//
/////////////////////////////////////////////////////////////////////////////

#include<math.h>
class Vector2 {
public:
	// Public representation: Not many options here.
	float x, y;
	// Constructors
	// Default constructor leaves vector in
	// an indeterminate state
	Vector2() {}
	// Copy constructor
	Vector2(const Vector2 &a) : x(a.x), y(a.y) {}
	// Construct given three values
	Vector2(float nx, float ny) : x(nx), y(ny) {}
	// Standard object maintenance
	// Assignment. We adhere to C convention and
	// return reference to the lvalue
	Vector2 &operator =(const Vector2 &a) {
		x = a.x; y = a.y;
		return *this;
	}
	// Check for equality
	bool operator ==(const Vector2 &a) const {
		return x == a.x && y == a.y;
	}


	bool operator !=(const Vector2 &a) const {
		return x != a.x || y != a.y;
	}


	// Vector operations
	// Set the vector to zero
	void zero() { x = y = 0.0f; }

	// Unary minus returns the negative of the vector
	Vector2 operator -() const { return Vector2(-x, -y); }

	// Binary + and � add and subtract vectors
	Vector2 operator +(const Vector2 &a) const {
		return Vector2(x + a.x, y + a.y);
	}

	Vector2 operator -(const Vector2 &a) const {
		return Vector2(x - a.x, y - a.y);
	}

	// Multiplication and division by scalar
	Vector2 operator *(float a) const {
		return Vector2(x*a, y*a);
	}
	Vector2 operator /(float a) const {
		float oneOverA = 1.0f / a; // NOTE: no check for divide by zero here
		return Vector2(x*oneOverA, y*oneOverA);
	}
	// Combined assignment operators to conform to
	// C notation convention
	Vector2 & operator +=(const Vector2 &a) {
		x += a.x; y += a.y;
		return *this;
	}
	Vector2 &operator -= (const Vector2 &a) {
		x -= a.x; y -= a.y;
		return *this;
	}
	Vector2 &operator *=(float a) {
		x *= a; y *= a;
		return *this;
	}
	Vector2 &operator /=(float a) {
		float oneOverA = 1.0f / a;
		x *= oneOverA; y *= oneOverA;
		return *this;
	}


	// Normalize the vector
	void normalize() {
		float magSq = x * x + y * y;
		if (magSq > 0.0f) { // check for divide-by-zero
			float oneOverMag = 1.0f / sqrt(magSq);
			x *= oneOverMag;
			y *= oneOverMag;
		}
	}
	// Vector dot product. We overload the standard
	// multiplication symbol to do this
	float operator *(const Vector2 &a) const {
		return x * a.x + y * a.y;
	}
};

