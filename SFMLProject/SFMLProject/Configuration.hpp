#pragma once
#include "ResourceManager.hpp"
#include "ActionMap.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"


class Player;
class Configuration
{
public:
	Configuration() = delete;
	Configuration(const Configuration&) = delete;
	Configuration& operator=(const Configuration&) = delete;

     enum Textures : int {PlayerShip,
		 PlayerLife,
		 BigSaucer,
		 SmallSaucer,
		 BigMeteor1,
		 BigMeteor2,
		 BigMeteor3,
		 BigMeteor4,
		 MediumMeteor1,
		 MediumMeteor2,
		 SmallMeteor1,
		 SmallMeteor2,
		 SmallMeteor3,
		 SmallMeteor4,
		 ShootPlayer,
		 ShootSaucer,
	 };

	static ResourceManager<sf::Texture, int> textures;

	enum Sounds : int {
		LaserPlayer,
		LaserEnemy,
		SaucerSpawn1,
		SaucerSpawn2,
		Boom,
		Boom2,
		Explosion1,
		Explosion2,
		Explosion3,
		Jump,
	};
	static ResourceManager<sf::SoundBuffer, int> sounds;


	enum Fonts : int { Gui };
	static ResourceManager<sf::Font, int> fonts;

	enum Musics : int {
		Theme
	};
	static ResourceManager<sf::Music, int> musics;



	enum PlayerInputs: int {Up,Left,Right,W,A,D, Shoot,HyperSpace};
	static ActionMap<int> _playerInputs;

	static void initialize();
	static void reset();

	static bool isGameOver();


	static void addScore(int s);
	static int getScore();
	static int level;
	static int lives;

	static Player* player;
	static void draw(sf::RenderTarget& target);
private:
	static int _score;
	static sf::Text   _txt_score;
	static sf::Sprite _spr_life;

	static void initTextures();

	static void initFonts();
	static void initSounds();
	static void initMusics();

	static void initPlayerInputs();
	
};
