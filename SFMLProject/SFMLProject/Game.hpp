#pragma once

#include <SFML/Graphics.hpp>
#include "World.hpp"
#include <SFML/Window.hpp>



class Player;

class Game
{
public:
	Game(const Game&) = delete;
	Game& operator=(const Game&) = delete;
	Game(int x = 1600, int y = 900);
	void run(int frame_per_seconds =30);

	void initLevel();



private:
	void processEvents();
	void update(sf::Time deltaTime);
	void render();

	void reset();

	sf::RenderWindow _window;
	World _world;

	sf::Time   _nextSaucer;
	sf::Text   _txt;
};

