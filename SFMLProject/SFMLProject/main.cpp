


#include <lua.hpp>
#include <stdio.h>

#include "Game.hpp"
#include "Configuration.hpp"
#include "ActionMap.hpp"


 int main()
{

	 Configuration::initialize();
	 Game game(800,600);

	lua_State* L;
	L = luaL_newstate();
	luaL_openlibs(L);
	luaL_dofile(L, "test.lua");
	lua_close(L);
	

   	game.run(30);
	
	return 0;
}