#ifndef BOOK_SAUCER_HPP
#define BOOK_SAUCER_HPP


#include "Enemy.hpp"
#include  <cfloat>

class Saucer : public Enemy
{
public:
	Saucer(const Saucer&) = delete;
	Saucer& operator=(const Entity&) = delete;
	Saucer(Configuration::Textures textures, World& world);
	using Enemy::Enemy;

	virtual bool isCollide(const Entity& other) const;
	virtual void update(sf::Time deltaTime);
	virtual void OnDestroy();
	static void newSaucer(World & World);

};

class BigSaucer :public Saucer
{
public:
	BigSaucer(World & world);
	virtual int getPoints() const;
private:
	sf::Time _timeSinceLastShoot;
};


class SmallSaucer :public Saucer
{
public:
	SmallSaucer(World & world);
	virtual int getPoints() const;
	virtual void update(sf::Time deltaTime);
private:
	sf::Time _timeSinceLastShoot;
};


#endif