#include "Player.hpp"
#include <cmath>

#include "Configuration.hpp"
#include "Collision.hpp"
#include "World.hpp"
#include "Shoot.hpp"
#include "random.hpp"


Player::Player(World & world) :Entity(Configuration::Textures::PlayerShip, world), ActionTarget(Configuration::_playerInputs), _isMoving(false), _rotation(0)
{
	
	bind(Configuration::PlayerInputs::Up, [this](const sf::Event &) {
		_isMoving = true;
	});

	bind(Configuration::PlayerInputs::Left, [this](const sf::Event &) {
		_rotation -= 1;
	});

	bind(Configuration::PlayerInputs::Right, [this](const sf::Event &) {
		_rotation += 1;
	});

	bind(Configuration::PlayerInputs::Shoot, [this](const sf::Event&) {
		shoot();
		//printf("Shooting");
	});


	bind(Configuration::PlayerInputs::HyperSpace, [this](const sf::Event&) {
		goToHyperSpace();
	});

}

bool Player::isCollide(const Entity& other) const
{
	if (dynamic_cast<const ShootPlayer*>(&other) == nullptr)
	{
		return Collision::circleTest(_sprite, other._sprite);
	}
	return false;
}


void Player::shoot() {
	
	//printf("Shhot");
	if (_timeSinceLastShoot > sf::seconds(0.3))
	{
		_world.add(new ShootPlayer(*this));
		_timeSinceLastShoot = sf::Time::Zero;
	}
}

void Player::goToHyperSpace() {
	_impulse = sf::Vector2f(0, 0);
	setPosition(random(0, _world.getX()), random(0, _world.getY()));
	_world.add(Configuration::Sounds::Jump);
}

void Player::update(sf::Time deltaTime)
{
	float seconds = deltaTime.asSeconds();
	_timeSinceLastShoot += deltaTime;
	if (_rotation != 0) {
		float angle = _rotation * 250 * seconds;
		_sprite.rotate(angle);
	}
	if (_isMoving)
	{
		float angle = _sprite.getRotation() / 180 * 3.14f - 3.14f / 2;
		_impulse += sf::Vector2f(std::cos(angle), std::sin(angle))* 300.f * seconds;

	}
	_sprite.move(seconds * _impulse);
}

void Player::processEvents() {
	_rotation = 0;
	_isMoving = false;
	ActionTarget::processEvents();
}


void Player::onDestroy() {
	//printf("player on destroyd\n");
	Entity::onDestroy();
	Configuration::lives--;
	Configuration::player = nullptr;
	_world.add(Configuration::Sounds::Boom);
}

