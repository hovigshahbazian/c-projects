#pragma once
#include <unordered_map>
#include "Action.hpp"

template<typename G = int>
class ActionMap
{
public:
	ActionMap(const ActionMap<G>&) = delete;
	ActionMap<G>& operator=(const ActionMap<G>&) = delete;

	ActionMap() = default;

	void map(const G& key, const Action& action);
	const Action& get(const G& key) const;

private:
	std::unordered_map<G, Action> _map;
};


template<typename G>
void ActionMap<G>::map(const G& key, const Action & action)
{
	_map.emplace(key, action);
}

template<typename G>
const Action& ActionMap<G>::get(const G& key) const
{
	return _map.at(key);
}

