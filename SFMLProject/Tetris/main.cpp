#include <lua.hpp>
#include <stdio.h>

#include "Game.h"

#include <Box2D/Box2D.h>


int main()
{

	Game game;
	game.run(60);

	lua_State* L;
	L = luaL_newstate();
	luaL_openlibs(L);
	luaL_dofile(L, "test.lua");
	lua_close(L);

	system("pause");


	return 0;
}